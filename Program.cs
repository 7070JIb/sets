﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mnozhestva
{
    class Program
    {
        public static Dictionary<char, List<string>> sets;
        public static List<string> allSigns;
        public const string INTER = "&";
        public const string PLUS = "|";
        public const string MINUS = "/";
        public const string SIMM = "\\";
        public const string ADDON = "-";
        static void Main(string[] args)
        {
            sets = new Dictionary<char, List<string>>();

            // Пустое множество под знаком 0
            sets.Add('0', new List<string>());
            // Все значения под знаком 1
            sets.Add('1', new List<string>());

            allSigns = new List<string>();
            InputSets();
            sets['1'] = sets.Aggregate((a, b) => new KeyValuePair<char, List<string>>(' ', a.Value.Concat(b.Value).Distinct().ToList())).Value;
            InputFormulas();
        }

        private static void InputFormulas()
        {
            Console.WriteLine("Ввод формул. Обозначения для операций: ");
            Console.WriteLine("\"" + INTER + "\"\t - пересечение");
            Console.WriteLine("\"" + PLUS + "\"\t - объединение");
            Console.WriteLine("\"" + MINUS + "\"\t - разность");
            Console.WriteLine("\"" + SIMM + "\"\t - симметрическая разность");
            Console.WriteLine("\"" + ADDON + "\"\t - дополнение");

            allSigns.AddRange(new string[] { INTER, PLUS, MINUS, SIMM, ADDON, "(", ")" });
            var IsFormulaCorrect = true;
            var formulaStr = "";
            do
            {
                formulaStr = Console.ReadLine().Replace(" ", "").Trim(' ');
                if (formulaStr.Count(t => t == '(') != formulaStr.Count(t => t == ')'))
                {
                    Console.WriteLine("ОШИБКА ВВОДА ФОРМУЛЫ: количество закрывающих скобок должно быть равно количеству открывающих!");
                    IsFormulaCorrect = true;
                }
                else
                {
                    IsFormulaCorrect = CreateFormula(formulaStr);
                }
            }
            while (formulaStr != "конец");
            Console.ReadLine();

        }

        private static bool CreateFormula(string formula)
        {
            var signs = new List<string>();
            var sign = "";
            for (int i = 0; i < formula.Length; i++)
            {
                sign += formula[i];
                if (allSigns.Contains(sign))
                {
                    signs.Add(new string(sign.ToCharArray()));
                    sign = "";
                }
                else
                {
                    Console.WriteLine("ОШИБКА ВВОДА ФОРМУЛЫ: неопознанное слово \"" + sign + "\"!");
                    return false;
                }
            }

            List<int> values;
            if (!SetValuesOverSignsAndRemoveBrackets(ref signs, out values))
            {
                return false;
            }

            List<string> result = Calculate(signs, values);
            var resultString = (result.Count == 0 || result == null) ? "[Пустое множество]" : result.Aggregate((a, b) => $"{a} , {b}") ;
            Console.WriteLine("-> {" + resultString +"}");
            return true;
        }

        private static bool SetValuesOverSignsAndRemoveBrackets(ref List<string> signs, out List<int> values)
        {
            values = new List<int>();
            var signsNoBrackets = new List<string>();
            int brackets = 0;
            bool IsPreviusASet = false;
            for (int i = 0; i < signs.Count; i++)
            {
                switch (signs[i])
                {
                    case "(":
                        brackets++;
                        break;
                    case ")":
                        brackets--;
                        break;
                    case PLUS:
                        if (!IsPreviusASet)
                        {
                            Console.WriteLine("ОШИБКА ВВОДА ФОРМУЛЫ: Перед знаками \"" + INTER + "\",\"" + PLUS + "\",\"" + MINUS + "\"" +
                                " или\"" + SIMM + "\" должно стоять множество");
                            return false;
                        }
                        values.Add(1 + 4 * brackets);
                        signsNoBrackets.Add(signs[i]);
                        IsPreviusASet = false;
                        break;
                    case MINUS:
                        goto case "|";
                    case INTER:
                        if (!IsPreviusASet)
                        {
                            Console.WriteLine("ОШИБКА ВВОДА ФОРМУЛЫ: Перед знаками \"" + INTER + "\",\"" + PLUS + "\",\"" + MINUS + "\"" +
                                " или\"" + SIMM + "\" должно стоять множество");
                            return false;
                        }
                        values.Add(2 + 4 * brackets);
                        signsNoBrackets.Add(signs[i]);
                        IsPreviusASet = false;
                        break;
                    case SIMM:
                        goto case "&";
                    case ADDON:
                        if (IsPreviusASet)
                        {
                            Console.WriteLine("ОШИБКА ВВОДА ФОРМУЛЫ: Перед знаком \"" + ADDON + "\" не должно стоять множество");
                            return false;
                        }
                        //Добавим в формулу перед минусом множество всех значений
                        values.Add(4 + 4 * brackets);
                        signsNoBrackets.Add("1");

                        values.Add(3 + 4 * brackets);
                        signsNoBrackets.Add(signs[i]);
                        IsPreviusASet = false;
                        break;
                    default:
                        if (IsPreviusASet)
                        {
                            Console.WriteLine("ОШИБКА ВВОДА ФОРМУЛЫ: Множество " + signs[i - 1] + " должно быть разделено оператором со множеством " + signs[i]);
                            return false;
                        }
                        values.Add(4 + 4 * brackets);
                        signsNoBrackets.Add(signs[i]);
                        IsPreviusASet = true;
                        break;
                }
            }
            signs = signsNoBrackets;
            return true;
        }

        private static List<string> Calculate(List<string> signs, List<int> values)
        {
            // Оператор - последнее слово с минимальным приоритетом
            var oper = signs[values.LastIndexOf(values.Min())];
            // Если в signs осталось только множество
            if (IsSet(oper))
            {
                return sets[oper[0]];
            }
            else
            {
                // Левый операнд - все слова до оператора 
                int leftCount = values.LastIndexOf(values.Min());
                var leftSigns = signs.Take(leftCount).ToList();
                var leftValues = values.Take(leftCount).ToList();

                // Правый операнд - все слова после оператора
                int rightCount = signs.Count - values.LastIndexOf(values.Min()) - 1;
                var rightSigns = signs.Skip(leftCount + 1).Take(rightCount).ToList();
                var rightValues = values.Skip(leftCount + 1).Take(rightCount).ToList();

                var leftResult = Calculate(leftSigns, leftValues);
                var rightResult = Calculate(rightSigns, rightValues);


                switch (oper)
                {
                    case PLUS:
                        // Объединение последовательности и отбор уникальных значений
                        return leftResult.Concat(rightResult).Distinct().ToList();
                    case MINUS:
                        // Исключение
                        return leftResult.Except(rightResult).ToList();
                    case INTER:
                        // Пересечение
                        return leftResult.Intersect(rightResult).ToList();
                    case SIMM:
                        // Объединение минус пересечение
                        return leftResult.Concat(rightResult).Except(leftResult.Intersect(rightResult)).ToList();
                    case ADDON:
                        // Вычесть из множества всех элементов
                        return sets['1'].Except(rightResult).ToList();
                    default: return new List<string>();
                }
            }
        }

        private static bool IsSet(string sign)
        {
            if (((sign[0] <= 'Z') && (sign[0] >= 'A')) || (sign == "1"))
            {
                return true;
            }
            return false;
        }
        private static void InputSets()
        {
            var setLetter = 'A';
            Console.WriteLine("Ввод множеств в формате {a1,a2,..,an}. Чтобы прекратить ввод, введите \".\"");
            var setString = "";
            do
            {
                Console.Write(setLetter + ": ");
                setString = Console.ReadLine().Trim(' ');
                if (setString == ".")
                {
                    break;
                }
                else
                // Если нет открывающей скобки,
                // или закрывающая скобка находится до открывающей,
                // или нет закрывающей скобки
                // или скобки встречаются не по одному разу каждая,
                // то выводим ошибку
                if ((setString.IndexOf('{') == -1) || 
                    (setString.IndexOf('{') >= setString.IndexOf('}')) ||
                    (setString.IndexOf('}') == -1) ||
                    (setString.Count(t => t == '{') * setString.Count(t => t == '}') != 1))
                {
                    Console.WriteLine("ОШИБКА ФОРМАТА ВВОДА МНОЖЕСТВА: Множество должно быть" +
                        " заключено в фигурные скобки \"{}\"");
                }
                else
                {
                    setString = setString.Remove(0, setString.IndexOf('{') + 1);
                    setString = setString.Remove(setString.IndexOf('}'), setString.Length - setString.IndexOf('}'));
                    string[] elements = setString.Split(',').Select(t => t.Trim(' ')).Where(t=>t != "").ToArray();
                    if (elements.Distinct().Count() < elements.Count())
                    {
                        Console.WriteLine("ОШИБКА ФОРМАТА ВВОДА МНОЖЕСТВА: элементы множества не должны повторяться!");
                    }
                    else
                    {
                        sets.Add(setLetter, elements.ToList());
                        allSigns.Add(setLetter.ToString());
                        setLetter++;
                    }
                }
            }
            while (true);
        }
    }
}
